# Background

At [GO2-Markets GmbH](https://go2-markets.eu/) we are dealing with brokerage trading of environmental commodities. Such trading is the trading of Guarantee of Origin (GoO) Certificates (hence the name of the company as well). More info about what is GoO and how it works you can find  here [https://www.youtube.com/watch?v=CX8cUw4ziHw&t=](https://www.youtube.com/watch?v=CX8cUw4ziHw&t=).

***

Currently we are doing our day-to-day business the old-fashioned way - by phone and emails. Our goal is to provide our clients with a digitalised platform, an online exchange,  which will automate the process of trading the GoO Certificates. The platform that we are working on is a digital auction marketplace, focused on B2B clients,  where GoO certificates are being traded. For example, client A wants  to buy GoO Certificate for 200 GWh of energy, produced by solar panels and is able to pay up to 0.20 EUR/ MWh, clients B, C, D will compete with each other giving the best price. The auction has a deadline and when's over, the best clients win (winners are defined based on price and volume).

***

One of the challenges that we are facing is presenting the detailed information  of the auction, the certificate, all the offers and the winner offers.

#### YOUR TASK
is to present the data from this [source](http://www.mocky.io/v2/5e3bfb383000009b2e2149fa)*** in a meaningful and easy to understand way, using all the tools by your preference.
The data contains all the information that we want to present to the client:
- details about the auction
- details about the GoO certificate, a.k.a order
- details about the offers
- details abouth the winner-offers, a.k.a transactions

Ideally, your solution should include also some code, i.e. HTML, CSS, JavaScript.

# How to submit:
Send your solution to vp@go2-markets.com with subject "Task submission".

***

#### ***Data reference
1. auction - details about the auction
- auction_status - could be open or closed. Open means that the auction can still accept offers (bids).
- auction_type - open or blind. Open means that each offer could be seen by each auction attendee.
- deadline - when the auction is over. The deadline marks the end of the auction.
- deal_type - buying or selling. Are we buying or selling GoO certificates.
- start_price - at what price the auction starts.
2.  Order - these are the certificate details:
- volume - in MW/h - total volume for trading.
- vintage - this is the production year, or when this energy is going to be/ was produced.
- technologies -  the energy source -  wind, hydro, solar or biomass.
- origins - the producing countries.
- labels - the energy could be certified, TÜV for example.
- delivery_date - the date in form year/month when the certificate should be delivered.
- support - optional field indicating if the certificate should include any kind of support.
3. Offers/ Bids
- volume - offered volume in MW/h
- price - the offered price for the offered volume
- origins - the origin country
- technologies - the energy source -  wind, hydro, solar or biomass
4. Transactions - transactions are winner offers/ winner bids.
-  offer - details about the offer, see p.3
- tx_volume - how much is the total traded volume from the given offer.  Since the auctioneer could choose how much volume she would like to trade, the transaction could be also partial, i.e. the auctioneer choose to get 10 out of 20MW/h from your offer.

*the fields id and ref are identifiers to connect different entities across the platform*
